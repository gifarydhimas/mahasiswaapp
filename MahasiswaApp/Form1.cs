﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace MahasiswaApp
{
    public partial class Form1 : Form
    {
        private String connString = "datasource=127.0.0.1;port=3306;username=root;password=root;database=test;SslMode=none";
        private int currentId = -1;
        private int currentRow = -1;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnUpdate_1063_Click(object sender, EventArgs e)
        {
            if (currentId < 0)
            {
                MessageBox.Show("You must select row from the List", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (textFirstName_1063.Text.Length <= 0)
            {
                MessageBox.Show("First Name cannot be empty", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (textLastName_1063.Text.Length <= 0)
            {
                MessageBox.Show("Last Name cannot be empty", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (textAddress_1063.Text.Length <= 0)
            {
                MessageBox.Show("Address cannot be empty", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            MySqlConnection connection = new MySqlConnection(connString);
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE user SET `first_name` = @firstName, `last_name` = @lastName, `address` = @address WHERE `id` = @id;", connection);
                cmd.Parameters.AddWithValue("@firstName", textFirstName_1063.Text);
                cmd.Parameters.AddWithValue("@lastName", textLastName_1063.Text);
                cmd.Parameters.AddWithValue("@address", textAddress_1063.Text);
                cmd.Parameters.AddWithValue("@id", currentId);
                int affectedRows = cmd.ExecuteNonQuery();
                Console.WriteLine("Affected Rows: {0}", affectedRows);
                ResetTextBoxes();
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnRead_1063_Click(object sender, EventArgs e)
        {
            MySqlConnection connection = new MySqlConnection(connString);
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM user;", connection);
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    listView_1063.Items.Clear();
                    while (reader.Read())
                    {
                        Console.WriteLine(reader.GetString(0));
                        ListViewItem item = new ListViewItem(new string[] { reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3) });
                        listView_1063.Items.Add(item);
                    }
                }
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnCreate_1063_Click(object sender, EventArgs e)
        {
            if(textFirstName_1063.Text.Length <= 0)
            {
                MessageBox.Show("First Name cannot be empty", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (textLastName_1063.Text.Length <= 0)
            {
                MessageBox.Show("Last Name cannot be empty", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (textAddress_1063.Text.Length <= 0)
            {
                MessageBox.Show("Address cannot be empty", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            MySqlConnection connection = new MySqlConnection(connString);
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO user(first_name, last_name, address) VALUES (@firstName, @lastName, @address);", connection);
                cmd.Parameters.AddWithValue("@firstName", textFirstName_1063.Text);
                cmd.Parameters.AddWithValue("@lastName", textLastName_1063.Text);
                cmd.Parameters.AddWithValue("@address", textAddress_1063.Text);
                int affectedRows = cmd.ExecuteNonQuery();
                Console.WriteLine("Affected Rows: {0}", affectedRows);
                ResetTextBoxes();
            }
            finally
            {
                connection.Close();
            }
        }

        private void ResetTextBoxes()
        {
            textFirstName_1063.Text = string.Empty;
            textLastName_1063.Text = string.Empty;
            textAddress_1063.Text = string.Empty;
        }

        private void listView_1063_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listView_1063.SelectedItems.Count > 0)
            {
                ListViewItem item = listView_1063.SelectedItems[0];
                textFirstName_1063.Text = item.SubItems[1].Text;
                textLastName_1063.Text = item.SubItems[2].Text;
                textAddress_1063.Text = item.SubItems[3].Text;
                currentId = int.Parse(item.SubItems[0].Text);
                currentRow = item.Index;
            }
            else
            {
                ResetTextBoxes();
                currentId = -1;
                currentRow = -1;
            }
            

        }

        private void btnDelete_1063_Click(object sender, EventArgs e)
        {
            if (currentId < 0)
            {
                MessageBox.Show("You must select row from the List", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            MySqlConnection connection = new MySqlConnection(connString);
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM user WHERE `id` = @id;", connection);
                cmd.Parameters.AddWithValue("@id", currentId);
                int affectedRows = cmd.ExecuteNonQuery();
                Console.WriteLine("Affected Rows: {0}", affectedRows);
                ResetTextBoxes();
                listView_1063.Items.RemoveAt(currentRow);
                currentId = -1;
                currentRow = -1;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
