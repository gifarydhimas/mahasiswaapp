﻿namespace MahasiswaApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textFirstName_1063 = new System.Windows.Forms.TextBox();
            this.textLastName_1063 = new System.Windows.Forms.TextBox();
            this.textAddress_1063 = new System.Windows.Forms.TextBox();
            this.listView_1063 = new System.Windows.Forms.ListView();
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colFirstName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLastName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnUpdate_1063 = new System.Windows.Forms.Button();
            this.btnDelete_1063 = new System.Windows.Forms.Button();
            this.btnRead_1063 = new System.Windows.Forms.Button();
            this.btnCreate_1063 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Last Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Address";
            // 
            // textFirstName_1063
            // 
            this.textFirstName_1063.Location = new System.Drawing.Point(94, 22);
            this.textFirstName_1063.Name = "textFirstName_1063";
            this.textFirstName_1063.Size = new System.Drawing.Size(179, 22);
            this.textFirstName_1063.TabIndex = 0;
            // 
            // textLastName_1063
            // 
            this.textLastName_1063.Location = new System.Drawing.Point(94, 60);
            this.textLastName_1063.Name = "textLastName_1063";
            this.textLastName_1063.Size = new System.Drawing.Size(179, 22);
            this.textLastName_1063.TabIndex = 1;
            // 
            // textAddress_1063
            // 
            this.textAddress_1063.Location = new System.Drawing.Point(94, 99);
            this.textAddress_1063.Name = "textAddress_1063";
            this.textAddress_1063.Size = new System.Drawing.Size(179, 22);
            this.textAddress_1063.TabIndex = 2;
            // 
            // listView_1063
            // 
            this.listView_1063.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colID,
            this.colFirstName,
            this.colLastName,
            this.colAddress});
            this.listView_1063.Dock = System.Windows.Forms.DockStyle.Right;
            this.listView_1063.FullRowSelect = true;
            this.listView_1063.Location = new System.Drawing.Point(279, 0);
            this.listView_1063.MultiSelect = false;
            this.listView_1063.Name = "listView_1063";
            this.listView_1063.Size = new System.Drawing.Size(521, 450);
            this.listView_1063.TabIndex = 10;
            this.listView_1063.UseCompatibleStateImageBehavior = false;
            this.listView_1063.View = System.Windows.Forms.View.Details;
            this.listView_1063.SelectedIndexChanged += new System.EventHandler(this.listView_1063_SelectedIndexChanged);
            // 
            // colID
            // 
            this.colID.Text = "ID";
            // 
            // colFirstName
            // 
            this.colFirstName.Text = "First Name";
            this.colFirstName.Width = 106;
            // 
            // colLastName
            // 
            this.colLastName.Text = "Last Name";
            this.colLastName.Width = 96;
            // 
            // colAddress
            // 
            this.colAddress.Text = "Address";
            this.colAddress.Width = 133;
            // 
            // btnUpdate_1063
            // 
            this.btnUpdate_1063.Location = new System.Drawing.Point(12, 196);
            this.btnUpdate_1063.Name = "btnUpdate_1063";
            this.btnUpdate_1063.Size = new System.Drawing.Size(90, 37);
            this.btnUpdate_1063.TabIndex = 3;
            this.btnUpdate_1063.Text = "Update";
            this.btnUpdate_1063.UseVisualStyleBackColor = true;
            this.btnUpdate_1063.Click += new System.EventHandler(this.btnUpdate_1063_Click);
            // 
            // btnDelete_1063
            // 
            this.btnDelete_1063.Location = new System.Drawing.Point(108, 196);
            this.btnDelete_1063.Name = "btnDelete_1063";
            this.btnDelete_1063.Size = new System.Drawing.Size(90, 37);
            this.btnDelete_1063.TabIndex = 4;
            this.btnDelete_1063.Text = "Delete";
            this.btnDelete_1063.UseVisualStyleBackColor = true;
            this.btnDelete_1063.Click += new System.EventHandler(this.btnDelete_1063_Click);
            // 
            // btnRead_1063
            // 
            this.btnRead_1063.Location = new System.Drawing.Point(108, 239);
            this.btnRead_1063.Name = "btnRead_1063";
            this.btnRead_1063.Size = new System.Drawing.Size(90, 37);
            this.btnRead_1063.TabIndex = 6;
            this.btnRead_1063.Text = "Read";
            this.btnRead_1063.UseVisualStyleBackColor = true;
            this.btnRead_1063.Click += new System.EventHandler(this.btnRead_1063_Click);
            // 
            // btnCreate_1063
            // 
            this.btnCreate_1063.Location = new System.Drawing.Point(12, 239);
            this.btnCreate_1063.Name = "btnCreate_1063";
            this.btnCreate_1063.Size = new System.Drawing.Size(90, 37);
            this.btnCreate_1063.TabIndex = 5;
            this.btnCreate_1063.Text = "Create";
            this.btnCreate_1063.UseVisualStyleBackColor = true;
            this.btnCreate_1063.Click += new System.EventHandler(this.btnCreate_1063_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnCreate_1063);
            this.Controls.Add(this.btnRead_1063);
            this.Controls.Add(this.btnDelete_1063);
            this.Controls.Add(this.btnUpdate_1063);
            this.Controls.Add(this.listView_1063);
            this.Controls.Add(this.textAddress_1063);
            this.Controls.Add(this.textLastName_1063);
            this.Controls.Add(this.textFirstName_1063);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "BiodataApp";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textFirstName_1063;
        private System.Windows.Forms.TextBox textLastName_1063;
        private System.Windows.Forms.TextBox textAddress_1063;
        private System.Windows.Forms.ListView listView_1063;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colFirstName;
        private System.Windows.Forms.ColumnHeader colLastName;
        private System.Windows.Forms.ColumnHeader colAddress;
        private System.Windows.Forms.Button btnUpdate_1063;
        private System.Windows.Forms.Button btnDelete_1063;
        private System.Windows.Forms.Button btnRead_1063;
        private System.Windows.Forms.Button btnCreate_1063;
    }
}

